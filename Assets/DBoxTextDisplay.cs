﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DBoxTextDisplay : MonoBehaviour
{
    public float textSpeed = 0.25f;

    private bool advance = false;   // Used to skip the typing effect.
    private bool complete = false;
    public bool Complete
    {
        get { return complete; }
    }

    private Text uiText;
    
    void Awake()
    {
        uiText = GetComponentInChildren<Text>();
    }

    public void Advance()
    {
        advance = true;
    }

    public IEnumerator ShowText(string line)
    {
        int lineIndex = 0;

        complete = false;

        for (int i = 0; i < line.Length; i++)
        {
            if (advance)
            {
                // Skip the typing and show the entire text.
                advance = false;
                uiText.text = line;
                break;
            }

            string sub = line.Substring(0, ++lineIndex);    // Advance the text by 1.

            uiText.text = sub;

            yield return new WaitForSeconds(textSpeed);
        }

        complete = true;
    }
}
