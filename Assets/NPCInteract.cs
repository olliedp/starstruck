﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCInteract : MonoBehaviour
{
    public DBoxTextDisplay dBoxText;

    private bool playerInRange;
    private bool interact = false;
    private bool inDialogue = false;

    private int dialogueIndex = 0;

    private Dialogue npcDialogue;
    private PlayerController playerController;
    private SmoothCamera2D cameraController;

	// Use this for initialization
	void Awake ()
    {
        npcDialogue = GetComponent<Dialogue>();
	}

    void Start()
    {
        playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        cameraController = GameObject.FindWithTag("MainCamera").GetComponent<SmoothCamera2D>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!inDialogue)
        {
            // Check to see if Olee has interacted with this NPC.
            if (playerInRange && Input.GetKeyDown(KeyCode.E))
                interact = true;
        }
        else  
        // While interacting with NPC
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!dBoxText.Complete)
                    dBoxText.Advance();

                // Advance or end dialogue
                else if (dialogueIndex >= npcDialogue.lines.Count)
                {
                    // End the dialogue and return control to Olee.
                    dialogueIndex = 0;
                    inDialogue = false;
                    dBoxText.gameObject.SetActive(false);
                    playerController.Suspended = false;
                    cameraController.target = playerController.transform;
                }
                else
                    AdvanceDialogue();
            }
        }
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        // Check to see if Olee is near this NPC.
        if (coll.tag == "Player")
        {
            playerInRange = true;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        // Start the NPC's dialogue if it was interacted with.
        if (interact)
        {
            playerController.Suspended = true;
            cameraController.target = transform;
            dBoxText.gameObject.SetActive(true);
            AdvanceDialogue();
            inDialogue = true;
            interact = false;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        // Check if Olee is no longer in NPC's range.
        if (coll.tag == "Player")
            playerInRange = false;
    }

    private void AdvanceDialogue()
    {
        StartCoroutine(dBoxText.ShowText(npcDialogue.lines[dialogueIndex]));
        dialogueIndex++;
    }
}
