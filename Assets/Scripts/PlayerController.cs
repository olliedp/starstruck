﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float maxSpeed;

    private bool suspended = false;
    public bool Suspended
    {
        get { return suspended; }
        set { suspended = value; }
    }

    [SerializeField]
    private bool jumping = false;

    [SerializeField]
    private Vector2 velocity;

    private Rigidbody2D rigidbody;
    private CircleCollider2D collider;
    private Animator animator;

    private SpriteRenderer animationSprite;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        velocity = Vector2.zero;

        collider = GetComponent<CircleCollider2D>();
        animator = GetComponent<Animator>();

        animationSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (!suspended) // Not paused for dialogue
        {
            // If Olee is on the ground and spacebar is pressed, prepare to jump.
            // Actual jump code is in FixedUpdate.
            if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
            {
                jumping = true;
                animator.SetBool("Jumping", jumping);
            }

            // Flip sprite based on intended movement direction.
            if (Input.GetKeyDown(KeyCode.D))
                transform.localScale = new Vector3(1, 1, 1);
            else if (Input.GetKeyDown(KeyCode.A))
                transform.localScale = new Vector3(-1, 1, 1);
        }
    }
	// Update is called once per frame
	void FixedUpdate()
    {
        if (!suspended) // Not paused for dialogue
        {
            // Apply force to Olee for movement when movement keys are pressed.
            if (Input.GetKey(KeyCode.D) && velocity.sqrMagnitude < maxSpeed)
                rigidbody.AddForce(transform.right * moveSpeed);

            if (Input.GetKey(KeyCode.A) && velocity.sqrMagnitude < maxSpeed)
                rigidbody.AddForce(-transform.right * moveSpeed);

            // Apply jump force
            if (jumping)
            {
                rigidbody.AddForce(transform.up * 500, ForceMode2D.Impulse);
                jumping = false;
            }

            velocity = rigidbody.velocity;

            if (velocity.y < 0)
                animator.SetBool("Jumping", false);


            // Set animation parameters
            animator.SetFloat("VelX", Mathf.Abs(velocity.x));
            animator.SetFloat("VelY", velocity.y);
            animator.SetBool("Grounded", IsGrounded());
        }
    }

    private bool IsGrounded()
    {
        return Physics2D.Raycast(transform.position, -Vector2.up, collider.bounds.extents.y + 0.25f);
    }
}
